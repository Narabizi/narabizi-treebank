# Datasets Arabizi Extracted

## arabizi_common_crawl.txt

Arabizi extracted from Common Crawl with Fasttext. Post-processed (look at file 'postprocessing.py') + filtered with vocab

## arabizi_web_crawled.txt

Arabizi extracted from North-African web crawled dataset with Fasttext + SVM. It is intersection of both models + sentences predicted by SVM not by Fasttext (postprocessed + Vocab method)

## arabizi_extracted.txt

Last sample : arabizi_common_crawl.txt + arabizi_web_crawled.txt

## vocab.txt

Vocabulary used for post-processing. Keep only sentences which contains 1 word or more from the vocab. 
